package com.example.search;

import com.example.search.model.Result;
import com.example.search.searchEngine.SearchBuilder;
import com.example.search.searchEngine.impl.EBuy;
import com.example.search.searchEngine.impl.EObuwie;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tests {

    private SearchBuilder searchBuilder = new SearchBuilder();


    @Test
    public void test(){
        Elements elementsRegular;
        org.jsoup.nodes.Document doc;
        try {
            doc = Jsoup.connect("https://www.eobuwie.com.pl/meskie.html").get();
        elementsRegular= doc.select(".products-list__link");
        System.out.println(elementsRegular.get(1).text());
        System.out.println(elementsRegular.get(1).select(".products-list__special-price"));
        System.out.println(elementsRegular.get(1).select(".products-list__special-price").text());

        Assert.assertNotNull(doc);
        Assert.assertTrue(doc.childNodeSize()>0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        }

    @Test
    public void test2(){
        List<Result> resultList;
        resultList= searchBuilder.findAll();
        resultList.forEach(x->System.out.print(x.toString()+"\n"));

        Assert.assertNotNull(resultList);
        Assert.assertTrue(resultList.size()>0);

    }

//    different whitespaces
    @Test
    public void testDouble() {
//        Double d = EObuwie.parseDouble("279,00 zł");
//        Double d = EObuwie.parseDouble("279,00 zł");
//        System.out.println( "\\u" + Integer.toHexString(' ' | 0x10000).substring(1) );
//        Double = EObuwie.parseDouble("279,00 zł");
//
        List<String> string = new ArrayList<>(Arrays.asList("279,00 zł","279,00 zł","279,00 zł"));
        List<Double> duoble = new ArrayList<>(Arrays.asList(279.00,279.00,279.00));
        Assert.assertArrayEquals(duoble.toArray(),string.stream().map(EObuwie::parseDouble).toArray());
        }

    @Test
    public void testFindByName() {
        List<Result> resultList;
        resultList= searchBuilder.findResultsByName("mokasyny");
        resultList.forEach(x->System.out.print(x.toString()+"\n"));
        System.out.println("========================================\n");
        Assert.assertNotNull(resultList);
        Assert.assertTrue(resultList.size()>0);

        resultList.clear();
        resultList= searchBuilder.findResultsByName("timberland");
        resultList.forEach(x->System.out.print(x.toString()+"\n"));
        Assert.assertNotNull(resultList);
        Assert.assertTrue(resultList.size()>0);
    }

    @Test
    public void jsonParser(){
//        URL url = new URL("http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=DamianGr-a-PRD-c7f4c67ed-63558be2&GLOBAL-ID=EBAY-US&keywords=harry+potter&paginationInput.entriesPerPage=3")

        try {
//            org.w3c.dom.Document document = (EBuy.loadDocument(new URL("http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=DamianGr-a-PRD-c7f4c67ed-63558be2&GLOBAL-ID=EBAY-US&keywords=harry+potter&paginationInput.entriesPerPage=100")));
            Document document = (EBuy.loadDocument(new URL("http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByCategory" +
                    "&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=DamianGr-a-PRD-c7f4c67ed-63558be2" +
                    "&GLOBAL-ID=EBAY-US" +
                    "&categoryId=3034" +
                    "&paginationInput.entriesPerPage=50"+
                    "&paginationInput.pageNumber=2")));
//            NodeList nodeList = document.getElementsByTagName("item");
//            System.out.println(nodeList.item(0).getFirstChild().getNextSibling().getTextContent());
//            System.out.println(nodeList.getLength());
//            TransformerFactory tf = TransformerFactory.newInstance();
//            Transformer transformer = tf.newTransformer();
//            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
//            StringWriter writer = new StringWriter();
//            transformer.transform(new DOMSource(document), new StreamResult(writer));
//            String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
//            System.out.println(output);
//            Document doc = Jsoup.parse(output,"",Parser.xmlParser());
            for (org.jsoup.nodes.Element e : document.select("title")){
                System.out.println(e);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void ebayFindAll() {
        EBuy eBuy = new EBuy();
        List<Result> resultList= eBuy.findAll();
        for (Result result : resultList) {
            System.out.println(result.toString());
        }
        Assert.assertNotNull(resultList);
        Assert.assertTrue(resultList.size()>0);
    }

}

