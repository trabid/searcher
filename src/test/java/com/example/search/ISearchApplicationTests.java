package com.example.search;

import com.example.search.data.ResultRepository;
import com.example.search.model.Result;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
//@SpringBootTest
//@ContextConfiguration(classes = SearchApplication.class, loader = AnnotationConfigContextLoader.class)
@SpringBootTest(classes = SearchApplication.class)
@AutoConfigureMockMvc
public class ISearchApplicationTests {

    @Autowired
    private ElasticsearchTemplate es;

    @Autowired
    private ResultRepository resultRepository;

    @Before
    public void before() {
        es.deleteIndex(Result.class);
        es.createIndex(Result.class);
        es.putMapping(Result.class);
        es.refresh(Result.class);
    }

    @Test
    public void saveAndDelete(){
        Result result = new Result("shoes",120.00, "zalando","http://www.wp.com","Image");

        Result testResult = resultRepository.save(result);

        Assert.assertNotNull(resultRepository.findById("http://www.wp.com"));
        Assert.assertEquals(resultRepository.findById("http://www.wp.com").get().getName(),result.getName());
        Assert.assertEquals(testResult.getName(),result.getName());
        Assert.assertEquals(testResult.getWebShop(),result.getWebShop());

        resultRepository.deleteById("http://www.wp.com");
        Assert.assertFalse(resultRepository.findById("http://www.wp.com").isPresent());
    }

    @Test
    public void findById (){
        Result result = new Result("shoes",120.00, "zalando","http://www.wp.com","Image");
        resultRepository.save(result);
        Result testResult = resultRepository.findById("http://www.wp.com").get();

        Assert.assertNotNull(resultRepository.findById("http://www.wp.com"));
        Assert.assertEquals(resultRepository.findById("http://www.wp.com").get().getName(),result.getName());
        Assert.assertEquals(testResult.getName(),result.getName());
        Assert.assertEquals(testResult.getWebShop(),result.getWebShop());

        resultRepository.deleteById("http://www.wp.com");
        Assert.assertFalse(resultRepository.findById("http://www.wp.com").isPresent());
    }

    @Test
    public void findByName (){
        Result result = new Result("shoes",120.00, "zalando","http://www.wp.com","Image");
        resultRepository.save(result);
        Result testResult = resultRepository.findByName("shoes").get(0);

        Assert.assertNotNull(resultRepository.findById("http://www.wp.com"));
        Assert.assertEquals(resultRepository.findById("http://www.wp.com").get().getName(),result.getName());
        Assert.assertEquals(testResult.getName(),result.getName());
        Assert.assertEquals(testResult.getWebShop(),result.getWebShop());

        resultRepository.deleteById("http://www.wp.com");
        Assert.assertFalse(resultRepository.findById("http://www.wp.com").isPresent());
    }

    @Test
    public void findByWebshop (){
        Result result = new Result("shoes",120.00, "zalando","http://www.wp.com","Image");
        resultRepository.save(result);
        Result testResult = resultRepository.findByWebShop("zalando").get(0);

        Assert.assertNotNull(resultRepository.findById("http://www.wp.com"));
        Assert.assertEquals(resultRepository.findById("http://www.wp.com").get().getName(),result.getName());
        Assert.assertEquals(testResult.getName(),result.getName());
        Assert.assertEquals(testResult.getWebShop(),result.getWebShop());

        resultRepository.deleteById("http://www.wp.com");
        Assert.assertFalse(resultRepository.findById("http://www.wp.com").isPresent());
    }
}
