import React  from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './AppNavBar.css'

export default class AppNavbar extends React.Component{
    constructor(props){
        super(props);
        this.state= {
            isOpen:false
        };
            this.toggle=this.toggle.bind(this);
        }

        toggle(){
        this.setState({isOpen:!this.state.isOpen});
        }

        render(){
       return(
                <Navbar id="topnav" color="dark" dark  expand="md" fluid="true">
                    <NavbarBrand tag={Link} to="/">Home</NavbarBrand>
                    <NavbarToggler onClick={this.toggle}/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink
                                    href="https://pl.linkedin.com/in/damian-gr%C4%99da-485689147">@LinkedIn</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="https://bitbucket.org/trabid/searcher.git">@Bitbucket</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
            </Navbar>
       );
        }
}