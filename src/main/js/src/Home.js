import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import { Button, Container,Form, FormGroup,Input } from 'reactstrap';
import logo from './img/images.png'

class Home extends Component {
    constructor(props){
        super(props);
        this.handleChange=this.handleChange.bind(this);
        // this.updateSearch=this.updateSearch.bind(this);
    }

    handleChange(e) {
        this.props.onTextChange(e.target.value);
    }


    render() {
        const search = this.props.search;
        return (
            <div>
                <AppNavbar/>
                <Container fluid >
                    <Form class="center">
                        <FormGroup>
                            {/*<Label id="lab">SEARCH FOR SHOES</Label>*/}
                            <img class="center" src={logo} alt="logo" />

                            <Input type="text" placeholder="Search..." value={search} onChange={this.handleChange} />
                        </FormGroup>
                        <Link to="/list">
                        <Button  outline color="primary" class="center" >Search</Button>
                        </Link>
                    </Form>
                </Container>
            </div>
        );
    }
}

export default Home;