import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ResultList from './ResultList';

class App extends Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state={search:''}
    }

    handleChange(search){
        this.setState({search:search});
    }
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} render={(props)=> <Home {...props} search={this.state.search}  onTextChange={this.handleChange}   />} />
                    <Route path='/list' exact={true}
                           render={(props)=> <ResultList {...props}  search={this.state.search} />}  />
                </Switch>
            </Router>
        );
    }
}

export default App;