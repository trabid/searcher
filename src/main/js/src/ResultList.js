import React, {Component} from 'react';
import {Button, ButtonGroup, Container, Table, Card, CardBody, CardTitle, CardSubtitle, CardImg} from 'reactstrap';
import AppNavbar from './AppNavbar';
import './result.css';

class ResultList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            results: [],
            isLoading: true,
            search: null
        };
        // this.remove=this.remove.bind(this);
    }


    componentDidMount() {
        const search = this.props.search;
        console.log('propsss ' + search);
        this.setState({isLoading: true});
        if (search !== '') {

            var url = new URL('http://localhost:3000/api/list'),
                params = {searchModel: search}
            Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
            fetch(url)
                .then(response => response.json())
                .then(data => this.setState({results: data, isLoading: false}));
        } else {
            fetch('/api/list')
                .then(response => response.json())
                .then(data => this.setState({results: data, isLoading: false}));
        }
    }

    render() {
        const {results, isLoading} = this.state;

        if (isLoading) {
            return (<div>
                    <AppNavbar/>
                    <h1>
                    <p>Loading...</p>
                    </h1>
                </div>
            );
        }

        const resultList = results.map(result => {
            return <div class="result" key={result.name}>
                <Card>
                    <div class="imgg">
                        <CardImg top width="100%" high="100%" src={result.img} alt="Card image cap"/>
                    </div>
                    <CardBody>
                        <CardTitle>{result.price.toFixed(2)} PLN</CardTitle>
                        <div class="subtitle">
                            <CardSubtitle>{result.name}</CardSubtitle>
                        </div>
                        <div class="btn-div">
                            <a href={result.url} target="_blank">
                                <Button size="lg" color="primary" block>{result.webShop}</Button>
                            </a>
                        </div>
                    </CardBody>
                </Card>
            </div>
        });

        return (
            <div>
                <AppNavbar/>
                <Container>
                    <h1>Result List</h1>
                    <div>
                        {resultList}
                    </div>
                </Container>
            </div>
        );
    }
}

export default ResultList;
