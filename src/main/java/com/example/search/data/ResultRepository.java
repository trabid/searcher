package com.example.search.data;

import com.example.search.model.Result;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.List;


public interface ResultRepository extends ElasticsearchCrudRepository<Result,String> {

    List<Result> findByName(String name);
    List<Result> findByWebShop(String webshop);
}
