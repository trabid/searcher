package com.example.search.model.ebuy;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Price{
    private String currencyId;
    @JsonProperty("__value__")
    private double value;

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Price{" +
                "currencyId='" + currencyId + '\'' +
                ", value=" + value +
                '}';
    }
}
