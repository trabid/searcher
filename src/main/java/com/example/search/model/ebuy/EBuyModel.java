package com.example.search.model.ebuy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonRootName("_cb_findItemsByKeywords")
public class EBuyModel {

    @JsonProperty("_cb_findItemsByKeywords")
    private String findItemsByKeywordsResponse;
    private String searchResult;
    private List<Item> items;


    public String getFindItemsByKeywordsResponse() {
        return findItemsByKeywordsResponse;
    }

    public void setFindItemsByKeywordsResponse(String findItemsByKeywordsResponse) {
        this.findItemsByKeywordsResponse = findItemsByKeywordsResponse;
    }

    public String getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(String searchResult) {
        this.searchResult = searchResult;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "EBuyModel{" +
                "findItemsByKeywordsResponse='" + findItemsByKeywordsResponse + '\'' +
                ", searchResult='" + searchResult + '\'' +
                ", items=" + items +
                '}';
    }
}
