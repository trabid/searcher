package com.example.search.model.ebuy;

public class Item {
    private String autoPay;
    private String condition;
    private String country;
    private String galleryURL;
    private String globalId;
    private String isMultiVariationListing;
    private String viewItemURL;
    private String title;
    private SellingStatus sellingStatus;

    public String getAutoPay() {
        return autoPay;
    }

    public void setAutoPay(String autoPay) {
        this.autoPay = autoPay;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGalleryURL() {
        return galleryURL;
    }

    public void setGalleryURL(String galleryURL) {
        this.galleryURL = galleryURL;
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public String getIsMultiVariationListing() {
        return isMultiVariationListing;
    }

    public void setIsMultiVariationListing(String isMultiVariationListing) {
        this.isMultiVariationListing = isMultiVariationListing;
    }

    public String getViewItemURL() {
        return viewItemURL;
    }

    public void setViewItemURL(String viewItemURL) {
        this.viewItemURL = viewItemURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SellingStatus getSellingStatus() {
        return sellingStatus;
    }

    public void setSellingStatus(SellingStatus sellingStatus) {
        this.sellingStatus = sellingStatus;
    }

    @Override
    public String toString() {
        return "Item{" +
                "autoPay='" + autoPay + '\'' +
                ", condition='" + condition + '\'' +
                ", country='" + country + '\'' +
                ", galleryURL='" + galleryURL + '\'' +
                ", globalId='" + globalId + '\'' +
                ", isMultiVariationListing='" + isMultiVariationListing + '\'' +
                ", viewItemURL='" + viewItemURL + '\'' +
                ", title='" + title + '\'' +
                ", sellingStatus=" + sellingStatus +
                '}';
    }
}

