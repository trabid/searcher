package com.example.search.model.ebuy;

public class SellingStatus{
    private Price convertedCurrentPrice;
    private Price currentPrice;
    private String sellingState;
    private String timeLeft;

    public Price getConvertedCurrentPrice() {
        return convertedCurrentPrice;
    }

    public void setConvertedCurrentPrice(Price convertedCurrentPrice) {
        this.convertedCurrentPrice = convertedCurrentPrice;
    }

    public Price getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Price currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getSellingState() {
        return sellingState;
    }

    public void setSellingState(String sellingState) {
        this.sellingState = sellingState;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    @Override
    public String toString() {
        return "SellingStatus{" +
                "convertedCurrentPrice=" + convertedCurrentPrice +
                ", currentPrice=" + currentPrice +
                ", sellingState='" + sellingState + '\'' +
                ", timeLeft='" + timeLeft + '\'' +
                '}';
    }
}
