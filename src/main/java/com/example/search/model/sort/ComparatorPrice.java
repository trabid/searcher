package com.example.search.model.sort;

import com.example.search.model.Result;

import java.util.Comparator;

public class ComparatorPrice implements Comparator<Result> {
    @Override
    public int compare(Result r1,Result r2) {
        int comparePrice=r1.getPrice().compareTo(r2.getPrice());
        if(comparePrice==0){
            int compareName=r1.getName().compareTo(r2.getName());
            if(compareName==0){
                return r1.getWebShop().compareTo(r2.getName());
            }else {
                return compareName;
            }
        } else {
            return comparePrice;
        }
    }
}
