package com.example.search.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.validation.constraints.NotNull;

@Document(indexName = "search")
public class Result implements Comparable<Result>{

    @Id
    private String url;
    private String name;
    private Double price;
    private String webShop;
    private String img;

    public Result() {
    }

    public Result(String name, Double price, String webShop, String url, String img) {
        this.name = name;
        this.price = price;
        this.webShop = webShop;
        this.url = url;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getWebShop() {
        return webShop;
    }

    public void setWebShop(String webShop) {
        this.webShop = webShop;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Result{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", webShop='" + webShop + '\'' +
                ", url='" + url + '\'' +
                ", img='" + img + '\'' +
                '}';
    }

    @Override
    public int compareTo(@NotNull Result o) {
        int compareName=name.compareTo(o.name);
        if(compareName==0){
            int compareShop=webShop.compareTo(o.webShop);
            if(compareShop==0){
                return price.compareTo(o.price);
            }else {
                return compareShop;
            }
        }else{
            return compareName;
        }
    }
}
