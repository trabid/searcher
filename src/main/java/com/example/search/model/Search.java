package com.example.search.model;

import org.springframework.stereotype.Component;


public class Search {
    private String value;

    public Search() {
    }

    public Search(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ISearch{" +
                "value='" + value + '\'' +
                '}';
    }
}

