package com.example.search.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class ElasticConfig {

    @Value("${elasticsearch.home:/home/damian/elasticsearch/elasticsearch-6.4.2}")
    private String elasticHome;

    @Value("${elasticsearch.cluster.name:elasticsearch}")
    private String clusterName;

    @Bean
    public Client client(){
        Settings elasticSettings = Settings.builder()
                .put ("client.transport.sniff", true)
                .put ( "path.home", elasticHome)
                .put ( "cluster.name", clusterName).build();
        TransportClient client = new PreBuiltTransportClient(elasticSettings);
        try {
            client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return client;
    }

//    @Bean
//    public ElasticsearchOperations elasticSearchTemplate(){
//        return new ElasticsearchTemplate(client());
//    }
}
