package com.example.search.searchEngine;

import com.example.search.model.Result;

import java.util.List;

public interface ISearch {

    /**
     * Returns List all results without filtering
     * @return
     */
    List<Result> findAll();

    /**
     * Returns list of results filtered by name contains string.
     * @param contains string the name is filtering by
     * @return
     */
    List<Result> findByName(String contains);


}
