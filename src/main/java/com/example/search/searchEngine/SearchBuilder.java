package com.example.search.searchEngine;

import com.example.search.data.ResultRepository;
import com.example.search.model.Result;
import com.example.search.searchEngine.impl.EBuy;
import com.example.search.searchEngine.impl.EObuwie;
import com.example.search.searchEngine.impl.Zalando;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SearchBuilder {

    private ResultRepository resultRepository;

    @Autowired
    public SearchBuilder(ResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    public List<Result> findResultsByName(String contains) {
        List<Result> results = new ArrayList<>();
        results.addAll(new EObuwie().findByName(contains));
        results.addAll(new Zalando().findByName(contains));
        results.addAll(new EBuy().findByName(contains));
        return results;
    }

    public List<Result> findAll() {
        List<Result> results = new ArrayList<>();
        List<ISearch> iSearchList=new ArrayList<>();
        iSearchList.add(new EObuwie());
        iSearchList.add(new Zalando());
        iSearchList.add(new EBuy());
        iSearchList.forEach(x->results.addAll(x.findAll()));
//        results.addAll(new EObuwie().findAll());
//        results.addAll(new Zalando().findAll());
        return results;
    }

    @Scheduled(fixedDelay = 1000L*60*20)
    public void saveInElasticSearch(){
        resultRepository.deleteAll();
        resultRepository.saveAll(findAll());
    }
}
