package com.example.search.searchEngine.impl;

import com.example.search.model.Result;
import com.example.search.searchEngine.ISearch;

import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class EBuy implements ISearch {
    @Override
    public List<Result> findAll() {
        List<Result> results = new LinkedList<>();
        try {
            int page = 1;
            int size = 0;
            URL category = new URL("http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByCategory" +
                    "&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=DamianGr-a-PRD-c7f4c67ed-63558be2" +
                    "&GLOBAL-ID=EBAY-PL" +
                    "&categoryId=3034" +
                    "&paginationInput.entriesPerPage=100" +
                    "&paginationInput.pageNumber=" + page);
            while (page < 8) {
                org.jsoup.nodes.Document doc = loadDocument(category);
                for (org.jsoup.nodes.Element element : doc.select("item")) {
                    size++;
                    Result result = new Result();
                    result.setName(element.select("title").text());
                    result.setPrice(parseDouble(element.select("currentPrice").text()));
                    result.setUrl(element.select("viewItemURL").text());
                    result.setImg(element.select("galleryURL").text());
                    result.setWebShop("eBuy");
                    results.add(result);
                }
                page++;
            }
            System.out.println(size);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Result> findByName(String contains) {
        List<Result> results = new LinkedList<>();
        try {
            int page = 1;
            int size = 0;
            URL keyWords = new URL("http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords" +
                    "&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=DamianGr-a-PRD-c7f4c67ed-63558be2" +
                    "&GLOBAL-ID=EBAY-US" +
                    "&keywords=" + contains +
                    "&paginationInput.entriesPerPage=100" +
                    "&paginationInput.pageNumber=" + page);
            while (page < 3) {
                org.jsoup.nodes.Document doc = loadDocument(keyWords);
                for (org.jsoup.nodes.Element element : doc.select("item")) {
                    size++;
                    Result result = new Result();
                    result.setUrl(element.select("viewItemURL").text());
                    result.setImg(element.select("galleryURL").text());
                    result.setName(element.select("title").text());
                    result.setPrice(parseDouble(element.select("currentPrice").text()));
                    result.setWebShop("eBuy");
                    results.add(result);
                }
                page++;
            }
            System.out.println(size);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results.stream()
                .filter(x -> x.getName().toLowerCase().contains(contains.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static org.jsoup.nodes.Document loadDocument(URL url) throws Exception {
        return Jsoup.parse(new URL(url.toString()).openStream(), "UTF-8", "", Parser.xmlParser());
    }

    private double parseDouble(String value) {
        BigDecimal bd = new BigDecimal(Double.parseDouble(value) * 3.7);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}

