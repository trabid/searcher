package com.example.search.searchEngine.impl;

import com.example.search.model.Result;
import com.example.search.searchEngine.ISearch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EObuwie implements ISearch {

    @Override
    public List<Result> findByName(String contains) {
        List<Result> results = new ArrayList<>();
        int page = 0;
        try {
            while (page < 10) {
                Document doc = Jsoup.connect("https://www.eobuwie.com.pl/meskie.html?p=" + page).get();
                Elements elements = doc.select(".products-list__link");
                elements.forEach(x -> results.add(parseToResult(x)));
                page++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results
                .stream()
                .filter(x -> x.getName().toLowerCase().contains(contains.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Result> findAll() {
        List<Result> results = new ArrayList<>();
        try {
            Document doc = Jsoup.connect("https://www.eobuwie.com.pl/meskie.html").get();
            Elements elements = doc.select(".products-list__link");
            for (Element el : elements) {
                results.add(parseToResult(el));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }


    private Result parseToResult(Element element) {
        Result result = new Result();
        result.setName(element.attr("title"));
        result.setUrl("https://" + element.attr("href").replaceFirst("//", ""));
        result.setWebShop("EObuwie");
        result.setImg("https://" + element.select("img").attr("data-src").replaceFirst("//", ""));

        Elements el;
        if (element.getElementsByClass("products-list__regular-price").size() > 0)
            el = element.select("div.products-list__regular-price");
        else
            el = element.select("div.products-list__special-price");
        try {
            result.setPrice(parseDouble(el.text()));
        } catch (Exception e) {
            System.err.println(element);
            e.printStackTrace();
            result.setPrice(0.00);
        }
        return result;
    }

    public static double parseDouble(String string) {
        return Double.parseDouble(string.replaceAll("(\\u00a0)|(zł)|(\\u0020)", "").replaceAll(",", "."));
    }

}
