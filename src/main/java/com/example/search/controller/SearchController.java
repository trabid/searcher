package com.example.search.controller;

import com.example.search.data.ResultRepository;
import com.example.search.model.Result;
import com.example.search.model.Search;
import com.example.search.searchEngine.SearchBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

    private SearchBuilder searchBuilder;
    private ResultRepository resultRepository;

    @Autowired
    public SearchController( ResultRepository resultRepository, SearchBuilder searchBuilder) {
        this.resultRepository = resultRepository;
        this.searchBuilder = searchBuilder;
    }

    @PostMapping
    public String result(@ModelAttribute Search searchModel, Model model) {
        List<Result> resultList = new ArrayList<>();
        if (searchModel.getValue() != null && searchModel.getValue().length() > 0) {
            model.addAttribute("searchModel", searchModel);
//            Result result = new Result("a",3.90,"c");
            resultList.addAll(resultRepository.findByName(searchModel.getValue()));
            model.addAttribute("resultList", resultList);
            resultList.forEach(System.out::println);
            return "results";
        } else{
            resultList.addAll(searchBuilder.findAll());
            model.addAttribute("resultList", resultList);
            resultList.forEach(System.out::println);
            return "results";
        }
    }

    @GetMapping("/errors")
    public String error(){
        return "errors";
    }
}
