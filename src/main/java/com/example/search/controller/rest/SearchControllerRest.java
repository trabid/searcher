package com.example.search.controller.rest;


import com.example.search.data.ResultRepository;
import com.example.search.model.Result;
import com.example.search.model.sort.ComparatorPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/list")
public class SearchControllerRest {
    private ResultRepository resultRepository;

    @Autowired
    public SearchControllerRest(ResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Result> resultList(@RequestParam(defaultValue = "price") String orderBy, @RequestParam(required = false) String searchModel) {
        ArrayList<Result> results = new ArrayList<>();
        if (searchModel != null) {
            resultRepository.findByName(searchModel).iterator().forEachRemaining(results::add);
        } else {
            resultRepository.findAll().iterator().forEachRemaining((results::add));
        }
        if (orderBy != null) {
            switch (orderBy) {
                case "price":
                    results.sort(new ComparatorPrice());
                    break;
                case "shop":
                    results.sort(Comparator.comparing(Result::getWebShop));
                    break;
                case "name":
                    Collections.sort(results);
                    break;
            }
        }
        return results;
    }
}
