package com.example.search.controller;


import com.example.search.model.Search;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {


    @GetMapping("/")
    public String Home(Model model) {
        model.addAttribute("searchModel",new Search());
        model.addAttribute("helloMessage","Witaj w wyszukiwarce obuwia");
        return "index";
    }
}
