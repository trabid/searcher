package com.example.search.service;

import com.example.search.model.Result;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ResourceBundle;

@Deprecated
@Service
public class ResultService {

    List<Result> resultList;

    public void add(Result result){
        resultList.add(result);
    }

    public List<Result> getResultList() {
        return resultList;
    }

    public void setResultList(List<Result> resultList) {
        this.resultList = resultList;
    }

    public void addAll(List<Result> list){
        list.forEach(x->resultList.add(x));
    }
}
